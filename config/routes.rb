Rails.application.routes.draw do
  root to: 'pages#home'
  get '/not_yet', to: 'pages#not_yet', as: :not_yet
  resources :professors, except: [:new]
end
